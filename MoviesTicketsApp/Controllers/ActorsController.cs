﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesTicketsApp.Data;

namespace MoviesTicketsApp.Controllers
{
    public class ActorsController : Controller
    {
        private readonly AppDbContext _context;

        public ActorsController(AppDbContext context) =>
            _context = context;

        public async Task<IActionResult> Index()
        {
            var actors =await _context.Actors.ToListAsync();
            
            return View(actors);
        }
    }
}