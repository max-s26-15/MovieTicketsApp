﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesTicketsApp.Data;

namespace MoviesTicketsApp.Controllers
{
    public class MoviesController : Controller
    {
        private readonly AppDbContext _context;

        public MoviesController(AppDbContext context) =>
            _context = context;
        
        public async Task<IActionResult> Index()
        {
            var movies = await _context.Producers.ToListAsync();
        
            return View();
        }
    }
}