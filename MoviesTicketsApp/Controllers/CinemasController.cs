﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesTicketsApp.Data;

namespace MoviesTicketsApp.Controllers
{
    public class CinemasController : Controller
    {
        private readonly AppDbContext _context;

        public CinemasController(AppDbContext context) =>
            _context = context;
        
        public async Task<IActionResult> Index()
        {
            var cinemas = await _context.Producers.ToListAsync();
        
            return View();
        }
    }
}