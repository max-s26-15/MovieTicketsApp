﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesTicketsApp.Models
{
    public class Producer
    {
        [Key]
        public int ProducerId { get; set; }

        [Display(Name = "Profile Image")]
        public string ProfileImageUrl { get; set; }
        
        [Display(Name = "Full Name")]
        public  string FullName { get; set; }

        [Display(Name = "Biography")]
        public string Bio { get; set; }
  
        // Relations 
        public List<Movie> Movies { get; set; }
    }
}