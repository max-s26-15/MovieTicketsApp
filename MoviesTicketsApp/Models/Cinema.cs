﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesTicketsApp.Models
{
    public class Cinema
    {
        [Key]
        public int CinemaId { get; set; }
        
        public string CinemaLogo { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        
        // Relations
        public List<Movie> Movies { get; set; }
    }
}