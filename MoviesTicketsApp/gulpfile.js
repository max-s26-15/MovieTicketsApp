﻿"use strict";

let gulp = require('gulp');
let del = require('del');

let nodeRoot = './node_modules/';
let targetPath = './wwwroot/lib/';

let moveFiles = async () => {
    gulp.src(nodeRoot + "bootstrap/dist/*/*").pipe(gulp.dest(targetPath + "bootstrap/dist"));

    gulp.src(nodeRoot + "font-awesome/*/*").pipe(gulp.dest(targetPath + "font-awesome"));

    gulp.src(nodeRoot + "jquery/dist/*").pipe(gulp.dest(targetPath + "jquery/dist"));
};

gulp.task('default', moveFiles);

gulp.task('moveFiles', moveFiles);

gulp.task('clean', () => {
    return del([targetPath + "/**/*"])
});