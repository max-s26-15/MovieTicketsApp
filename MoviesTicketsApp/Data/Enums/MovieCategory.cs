﻿namespace MoviesTicketsApp.Data
{
    public enum MovieCategory
    {
        Action,
        Comedy,
        Drama,
        Documentary,
        Horror,
        Cartoon
    }
}